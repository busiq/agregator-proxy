require 'fields_list'
module ApiClient
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def project_field(key, value = nil)
      @project_fields_mapping ||= {}
      @project_fields_mapping[key] = value.nil? ? key.to_s : value
    end

    def project_fields_mapping
      @project_fields_mapping
    end
  end

  def fetch_projects(page); end

  def projects_list(page)
    fetch_projects(page).map do |project|
      map_project_attributes(project.with_indifferent_access)
    end
  end

  def map_project_attributes(project)
    Hash[self.class.project_fields_mapping.map{ |k,v| [k, v.is_a?(Proc) ? v.call(project) : project[v]] }]
  end

  def get_missing_fields
    {
      general: ApiClient::Fields::GENERAL - self.class.project_fields_mapping.keys,
      funding_general: ApiClient::Fields::FUNDING_GENERAL - self.class.project_fields_mapping.keys,
      equity: ApiClient::Fields::EQUITY - self.class.project_fields_mapping.keys,
      sample_raw_project: sample_project,
      sample_mapped_project: map_project_attributes(sample_project)
    }
  end

  private
  def sample_project
    @sample ||= fetch_projects(1).sample
  end
end
