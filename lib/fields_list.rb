module ApiClient
  module Fields
    GENERAL = %i(name picture_url summary currency busines_nature)
    FUNDING_GENERAL = %i(started_on ends_on target_amount target_amount)
    EQUITY = %i(company_type incorporated_on company_url markets locations invalidated_at)
  end
end
