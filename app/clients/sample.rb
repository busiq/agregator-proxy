require 'api_client'

module App
  module Clients
    class Sampleapiclient
      include ApiClient

      project_field :name, :project_name
      project_field :currency, :current_currency
      project_field :pucture_url, ->(project){ project[:hidden_image][:image_url] }

      def fetch_projects(page)
        [
          {
            project_name: 'Test',
            current_currency: 'EUR',
            hidden_image: {
              image_url: 'http://google.com'
            }
          },
          {
            project_name: 'Test2',
            current_currency: 'USD',
            hidden_image: {
              image_url: 'http://google.com'
            }
          }
        ]
      end

      def get_projects(page)
        projects_list(page)
      end
    end
  end
end
