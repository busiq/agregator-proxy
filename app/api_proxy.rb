require 'grape'

class ApiProxy < Grape::API
  format :json
  default_format :json

  helpers AuthHelpers
  before do
    throw :error, status: 401, message: "Unauthorized" unless valid_token?(params[:token])
  end

  resource ':proxy' do
    before do
      @proxy = "App::Clients::#{params[:proxy].capitalize}".constantize.new
    end

    get :projects_list do
      @proxy.get_projects(params[:page])
    end

    get :missing_fields do
      @proxy.get_missing_fields
    end
  end
end
