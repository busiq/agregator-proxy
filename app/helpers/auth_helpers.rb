module AuthHelpers
  def auth_token
    App.config['access_token']
  end

  def valid_token?(token)
    token == auth_token
  end
end
