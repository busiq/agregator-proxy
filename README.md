This is simple API-proxy app.

Basically all you need to define are classes for handling each API.

If want to see how it works look at sample.rb in `app/clients` dir.
You need to specify which fields corresponds to which in rails-app.
And then there are 2 methods required:
- fetch_projects - this should return projects from selected API
- get_projects - this will be available to rails app at `host/:apiname/get_projects`

All requests are simple protected by token (append `?token=value` to the url).
You can change the token in secrets.yml
