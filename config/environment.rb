require_relative 'app'
Dir[App.root.join('config/initializers/*.rb')].each { |f| require f }
require App.root.join("config/environments", App.env)
$: << App.root.join('lib')
Dir[App.root.join('app/*s/*.rb')].each { |f| require f }
