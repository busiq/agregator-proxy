require 'pathname'

module App
  class << self
    def env
      ENV['APP_ENV'] || 'development'
    end

    def root
      Pathname.pwd
    end

    def config
      @config ||= YAML.load(File.read(App.root.join('config/secrets.yml')))[App.env] rescue {} || {}
    end
  end
end
